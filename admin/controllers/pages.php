<?php
/**
 * @author       JoomWorker
 * @email        info@joomla.work
 * @url          http://www.joomla.work
 * @copyright    Copyright (c) 2010 - 2019 JoomWorker
 * @license      GNU General Public License version 2 or later
 * @date         2019/01/01 09:30
 */
//no direct accees
defined ('_JEXEC') or die ('Restricted access');

jimport('joomla.application.component.controlleradmin');

class JwpagefactoryControllerPages extends JControllerAdmin
{
	public function getModel($name = 'Page', $prefix = 'JwpagefactoryModel')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
        return $model;
	}
}
